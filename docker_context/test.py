import pytest

def capital_case(x):
    if not isinstance(x, str):
        raise TypeError('Please provide a string argument')
    return x.capitalize()

def test_capital_case():
    #f = open("/home/testing/demofile2.txt", "a")
    #f.write("Now the file has more content!")
    #f.close()
    assert capital_case('cica') == 'Cica'

def test_raises_exception_on_non_string_arguments():
    with pytest.raises(TypeError):
        capital_case(9)

test_capital_case()